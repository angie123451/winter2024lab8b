import java.util.Random;
public class Board{
	private  Tile[][] grid;
	private Random rand;
	
	
	//constructor
	public Board(){
		rand = new Random();
		final int gridSize=5;
		this.grid=new Tile[gridSize][gridSize];
		for(int i =0; i<grid.length;i++){
			int ran = rand.nextInt(grid.length);
			for (int j=0; j<grid[i].length;j++){
				if (j==ran){
					grid[i][j]= Tile.Hidden_Wall;
				}else{
					grid[i][j]= Tile.Blank;
				}
			}
		}
		
	}
	//toString
	public String toString(){
		String board="";
		for(int i =0; i<grid.length;i++){
			for (int j=0; j<grid[i].length;j++){
				board += grid[i][j].getName()+" ";
			}
			board += "\n";
		}
		return board;
	}
	
	
	// check if you are allow to make a move
	public int placeToken(int row, int col){
		
		if(0>row || row>=grid.length || 0>col||col>=grid.length){
			return -2;
		}else if(grid[row][col] == Tile.Castle || grid[row][col] == Tile.Wall){
			return -1;
		}else if(grid[row][col]==Tile.Hidden_Wall){
			//place wall if it is a wall
			grid[row][col]=Tile.Wall;
			return 1;
		}else{
			//place castle if it is a castle
			grid[row][col]=Tile.Castle;
			return 0;
		} 
		
	}
	
	/*Validator
	if(0<=row || row<grid.length || 0<=col||col<grid.length){
			throw new Illegal("placemenet outside of arr");
		}*/
}