import java.util.Scanner;
public class BoardGameApp{
	public static void main (String[]args){
		Scanner reader = new Scanner(System.in);
		Board board= new Board();
		System.out.println("Welcome to the Game");
		
		
		//loop
		int numCastle=7;
		int turn=0;
		while(numCastle>0 && turn <8){
			//initalize ur game
			System.out.println(board);
			System.out.println("nb of Castle: "+numCastle);
			System.out.println("nb of turn: "+turn);
			
			//ask user to input two number
			System.out.println("Please input a number row");
			int row= reader.nextInt();
			System.out.println("Please input a number column");
			int col= reader.nextInt();
			int result= board.placeToken(row,col);
			
			
				// re ask the user to input a number until valide
			while(result<0){
				System.out.println("Re-enter a new number");
				row= reader.nextInt();
				col= reader.nextInt();
				//put this here so that the result get updated
				result= board.placeToken(row,col);
			}
			//show result (wall or castle)
			if (result==1){
				System.out.println("There was a wall at this positions \n");
			}else if (result==0){
				System.out.println("Successfully placed \n");
				numCastle--;
			}
			turn++;
		}
		//check ur win
		if (numCastle==0){
			System.out.println("Bravo! you WON!");
		}else{
			
			System.out.println("Sorry, you lost");
		}
		
		
		
	}
}