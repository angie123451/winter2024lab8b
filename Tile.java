public enum Tile{
	Hidden_Wall("_"),
	Castle("C"),
	Wall("W"),
	Blank("_");
	
	private String name;
	
	private Tile(String name){
		this.name=name;
	}
	
	public String getName(){
		return this.name;
	}
}
